import json
import requests

"""
SRT (Simple Rest Test)
----------------------
Simple rest response checker.

Checks status of response, response as JSON.

Global objects/functions:
  rtResult - instance of result container. Contains fails of checks
  ANY - is simple string "*", used for JSON checking
  q( name ) - function that creates instance of query builder
  fail( failString ) - just add custom fail to rtResult
  done() - mandatory method that print summeries and _EXIT_ application (call 'exit()' )

Methods of query builder (RTQuery) :
  url( string ): self
  header( string, string ): self
  get(): ResponseChecker 
  post( data ): ResponseChecker
  put( data ): ResponseChecker
  delete(): ResponseChecker

Methods of response checker (RTResponse / RTFailResponse):
  status( int ): self
  notStatus( int ): self
  json( dict/list ): self
  notJson( dict/list ): self
  ok( fun() ): self   - Call fun() if not failed yet
  custom( fun(status, body): boolean )  - Call  fun() with status code and response body. If it returns False - fail.

If you want to check fails manually, just call 'rtResult.fails'. It contains list of string with fails.
' len(rtResult.fails) == 0 ' - means all checks are passed.

Example:

    from srt import *

    # Example of query builer:
    q("Name of test")   # Do nothing, but create instance of query builder  (RTQuery)
    q( ... ).url( "http://some_host")  # same as line above, but set internal variable 'url'
    q( ... ).url( ... ).header("HeaderOne", "Value").header("HeaderTwo", "2")  # same as line above, but store two headers for builder
    q( ... ).get()  # HTTP GET Query on default url ( http://localhost ), returns object for checks
    q( ... ).post( data )  # HTTP Post with payload 'data', return object for checks
    q( ... ).put( data ) # same as .post( data ), but other method
    q( ... ).delete()

    # Checks:
    q( ... ).get().status(200)  # Check if response status is 200
    q( ... ).get().notStatus(400).notStatus(500)  # Check if response status neither 400 nor 500
    q( ... ).post( ... ).json( {'x': 42} )  # Check if response is JSON and contains  'x' with value '42'
    q( ... ).post( ... ).json( {'x': ANY} )  # Same as above, but checks if JSON contains 'x' with any value
    q( ... ).post( ... ).notJson( {'x': 42} )  # Check if response is JSON and _NOT_ contains 'x' with value '42'
    q( ... ).put( ... ).custom( lambda status, responseData: responseData == "Hello, World!" )  # Custom check function. Check if response body is 'Hello, World!'
    q( ... ).delete( ... ).status(200).ok( lambda: print("Status is 200").json( {'x': 42}).ok( lambda: print("And json contains 'x'=42 ") )  # ok( myFun ) - 

    # some random checks:
    q("Name of test").url("http://localhost:8080/services/rest/v3/class").get().status(200).json( {"Code": "aap"} )
    q("Simple test: status is 200 and correct JSON").url("http://localhost:8080").get().status(200).json({"c": {"x": "*"}})
    q("Simple test| not json ").url("http://localhost:8080").get().status(200).notJson({"c": {"y": ANY}})
    q("Simple test of not status and not json ").url("http://localhost:8080").post({"x":42}).notStatus(400).notJson({"c": {"y": ANY}})
    q("Custom check").url( ... ).byFun( lambda status, data: return data == "OK" )  # custom lambda function on response

    done()  # Mandatory method for checking result

"""

class RTResult:
  """ Resulting class. """
  fails = []
  qCount = 0

  def incQuery(self):
    self.qCount += 1
  def fail(self, fail):
    self.fails.append(fail)
  def done(self):
    if len(self.fails) == 0:
      print("All OK")
      print("Success !")
      exit(0)
    print("Fails: ")
    for fail in self.fails:
      print("[FAIL] " + fail)
    print(" --- ")
    print("Failed:  " + str(len(self.fails)) + "/" + str(self.qCount))
    exit(1)

rtResult = RTResult()
def fail(fail): rtResult.fail( fail )



class RTFailResponse:
  name = ""
  response = None
  def __init__(self, name):
    rtResult.fail(name)
  def status(self, status): return self
  def notStatus(self, status): return self
  def json(self, json): return self
  def notJson(self, json): return self
  def custom(self, checkFun): return self
  def ok(self, okFun): return self
  def responseText(self):
    try:
      if 'response' in self:
        return self.response.text
    except (ValueError, KeyError, TypeError):
      pass
    return "-"


class RTResponse(RTFailResponse):
  def __init__(self, name, response):
    self.name = name
    self.response = response

  def fail(self, fail):
    fail = RTFailResponse(self.name + ": " + fail)
    fail.response = self.response
    return fail

  def status(self, status):
    if self.response.status_code != status:
      return self.fail("Expected status " + str(status) + ", but " + str( self.response.status_code ) )
    return self

  def notStatus(self, status):
    if self.response.status_code == status:
      return self.fail("Not expected status " + str(status) )
    return self

  def json(self, jsonData):
    try:
      respJson = json.loads( self.response.text )
      if not self._jsonEq( jsonData, respJson ):
        return self.fail("Expected JSON: " + str(jsonData) + "\n\t\tbut " + str(respJson) )
    except (ValueError, KeyError, TypeError):
      return self.fail("Response is not JSON: " + str(self.response.text) )
    return self

  def notJson(self, jsonData):
    try:
      respJson = json.loads( self.response.text )
      if self._jsonEq( jsonData, respJson ):
        return self.fail("NOT expected JSON: " + str(jsonData) + "\n\t\tbut " + str(respJson) )
    except (ValueError, KeyError, TypeError):
      return self.fail("Response is not JSON: " + str(self.response.text) )
    return self

  def custom(self, checkFun):
    if not checkFun( self.response.status_code, self.response.text ):
      return self.fail("Custom check failed")
    return self

  def ok(self, okFun):
    okFun()
    return self

  def responseText(self):
    try:
      return self.response.text
    except (ValueError, KeyError, TypeError):
      pass
    return "-"

  def _jsonEq(self, a, b):
    if isinstance(a, list):
      if not isinstance(b, list): return False
      for itemA in a:
        ok = False
        for itemB in b:
          if self._jsonEq(itemA, itemB): 
            ok = True
            break
        if not ok:
          return False
    elif isinstance(a, dict):
      if not isinstance(b, dict): return False
      for item in a:
        if item not in b: return False
        if not self._jsonEq(a[item], b[item]): return False
    elif a == "*":
      return True
    else:
      return a == b
    return True




class RTQuery:
  headers = {}
  url = "http://localhost:8080"

  def __init__(self, name):
    rtResult.incQuery()
    self.name = name

  def url(self, url):
    self.url = url
    return self

  def header(self, header, value): 
    self.headers[header] = value
    return self

  def _safe(self, qFun):
    try:
      return qFun()
    except Exception as cr:
      return RTFailResponse( self.name + " > HTTP request error: " + str(cr) )

  def get(self):
    return self._safe( lambda: RTResponse(self.name, requests.get(url=self.url, headers=self.headers) ) )

  def delete(self):
    return self._safe( lambda: RTResponse(self.name, requests.delete(url=self.url, headers=self.headers) ) )

  def post(self, data):
    return self._safe( lambda: RTResponse(self.name, requests.post(url=self.url, data=data, headers=self.headers) ) )

  def put(self, data):
    return self._safe( lambda: RTResponse(self.name, requests.post(url=self.url, data=data, headers=self.headers) ) )


def q(name): return RTQuery(name)
def done(): rtResult.done()
ANY = "*"

