from srt import *

def x(txt):
  print(txt)


q("Simple test").url("http://localhost:8080").get()
q("Simple test 200 {'x'} ").url("http://localhost:8080").get().status(200).ok( lambda: x("[PASS] Simple test 200. Niiice") )
q("Simple test 200 {'x'} ").url("http://localhost:8080").get().status(200).notJson({"c": {"y": ANY}})
q("Simple test 200 {'x'} ").url("http://localhost:8080").get().notStatus(200)
q("Test calling of custom check").url("http://localhost:8080").header("xxx", "yyy").post(str({"x":42})).custom(lambda s, b: s != 200)

done()
